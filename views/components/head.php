<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ewilan Rivière</title>
    
    <!-- Icon -->
    <link rel="shortcut icon" href="assets/images/ewilan-riviere-logo-white.png">
    <!-- Materialize CSS  -->
    <link rel="stylesheet" href="assets/css/materialize-css/materialize.min.css">
    <link rel="stylesheet" href="assets/css/materialize-css/fonts.css">
    <!-- Personnal CSS Stylesheets  -->
    <link rel="stylesheet" href="assets/css/bootstrap-light.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <!-- Font Awesome  -->
    <script src="assets/js/font-awesome/font-awesome.min.js"></script>

    <script src="assets/js/utilitary.js"></script>
    <script>
        function navbarActive(navbarID) {
            var navLink = byClass('nav-link');
            for (let index = 0; index < navLink.length; index++) {
                const element = navLink[index];
                element.classList.remove("active");
            }
            var navLinkID = bySelect('#'+navbarID);
            for (let index = 0; index < navLinkID.length; index++) {
                const element = navLinkID[index];
                element.classList.add("active");
            }
        }
    </script>
</head>
    <body class="parallax">