<div class="navbar-fixed">
    <nav class="grey lighten-5 navbar">
        <div class="nav-wrapper">
            <div class="brand-logo title ml-2">
                <a href="/">
                    <img src="assets/images/ewilan-riviere-logo-white.png" alt="" class="logo">
                </a>
            </div>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger grey-text text-darken-4 px-2"><i class="material-icons">menu</i></a>
            <ul id="navbarClassic" class="right hide-on-med-and-down">
            </ul>
        </div>
    </nav>
</div>

<ul id="mobile-demo" class="sidenav navbar">
    <li>
		<div class="user-view">
			<a><img class="circle" src="assets/images/ewilan_logo.png"></a>
			<a><span class="grey-text text-darken-4 name">Ewilan Rivière</span></a>
			<a href="mailto:ewilan@dotslashplay.it"><span class="grey-text text-darken-4 email">ewilan@dotslashplay.it</span></a>
		</div>
	</li>
    <li><div class="divider"></div></li>
    <li><a class="subheader morpheus grey-text text-darken-4">Ewilan Rivière</a></li>
    <ul id="navbarSide">
    </ul>
</ul>