<main id="aboutPage" class="vertical-centered not-visible">
    <div class="">
        <div class="row">
            <div class="col s12">
                <ul class="tabs">
                    <li class="tab col s6">
                        <a href="#profil" class="active font-weight-bold black-text">Profil</a>
                    </li>
                    <li class="tab col s6">
                        <a href="#links" class="font-weight-bold black-text">Liens de ce domaine</a>
                    </li>
                </ul>
            </div>
            <div id="profil" class="col s12">
                <div class="mt-4">
                    <div>
                        <h4 class="text-center font-weight-bold">Projets</h4>
                    </div>
                    <div id="aboutProfilGit" class="d-flex justify-content-center">
                        <!-- Generating aboutProfilGit  -->
                    </div>
                </div>
                <div id="aboutProfilPro" class="mt-4 d-flex justify-content-around">
                    <!-- Generating aboutProfilPro  -->
                </div>
            </div>
            <div id="links" class="col s12">
                <div class="mt-4">
                    <ul class="collection">
                        <li class="collection-item avatar">
                            <i class="material-icons circle purple">code</i>
                            <span class="title">Domaine principal</span>
                            <p id="domainLinksMain">
                                <!-- Generating domainLinksMain  -->
                            </p>
                        </li>
                        <li class="collection-item avatar">
                            <i class="material-icons circle">school</i>
                            <span class="title">Site de la promo 3 de la Code Académie</span>
                            <p id="domainLinksPromo">
                                <!-- Generating domainLinksPromo  -->
                            </p>
                        </li>
                        <li class="collection-item avatar">
                            <i class="material-icons circle green">movie</i>
                            <span class="title">Hacked Movies</span>
                            <p id="domainLinksMovies">
                                <!-- Generating domainLinksMovies  -->
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</main>