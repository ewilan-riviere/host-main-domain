<div class="container">
    <div class="row">
        <div class="col s12 m10">
            <div id="introduction" class="section scrollspy">
                <div class="flex-center position-ref full-height">
                    <main class="content">
                        <div class="horizontal-centered">
                            <h1 class="morpheus mb-5">
                                <a
                                    href="http://portfolio.ewilan-riviere.tech/"
                                    target="_blank"
                                    class="tooltipped black-text"
                                    rel="noopener noreferrer"
                                    data-position="top"
                                    data-tooltip="Voir mon portfolio"
                                >
                                    <u>Ewilan Rivière</u>
                                </a>
                            </h1>
                            <div>
                                <i>Code Is Coming...</i>
                            </div>
                            <blockquote class="d-flex">
                                <div class="citation-image-container">
                                    <img src="assets/images/crown.png" alt="" class="citation-image">
                                </div>
                                <div class="ml-4">
                                    <i>Developpers made front, and it delights me. Developpers made back, and it delights me. When it comes 
                                        to code I fight for PHP, when it comes to design... I don't choose sides.<br>- Ewilan Martell, Game 
                                        of Devs</i>
                                </div>
                            </blockquote>
                            <main id="aboutSocial" class="row">
                                <!-- Generate social links  -->
                            </main>
                            <main id="aboutGit">
                                <!-- Generate social links  -->
                            </main>
                        </div>
                    </main>
                </div>
            </div>
            
                <div id="domaines" class="section scrollspy">
                    <div class="flex-center position-ref">
                        <main class="content">
                            <div class="horizontal-centered">
                                <h2 class="morpheus mb-5">Sous-domaines</h2>
                                <ul id="domainLinks" class="collection">
                                    <!-- Generate domain links  -->
                                </ul>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
            <div class="col hide-on-extrasmall s2">
                <ul class="section table-of-contents scrollspy-menu">
                    <li>
                        <a href="#introduction">
                            Introduction
                        </a>
                    </li>
                    <li>
                        <a href="#domaines">
                            Sous-domaines
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>