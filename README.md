# Host Main Domain

![](assets/images/crown-readme.png)

[![PHP 7.2](https://img.shields.io/badge/PHP-7.2-blue)](https://www.php.net)
[![NodeJS 11.15](https://img.shields.io/badge/NodeJS-11.15-green)](https://nodejs.org/en)

**Deploy on [ewilan-riviere.tech](https://ewilan-riviere.tech/)**

---
## **1. About**

Display a SPA with no framework to present all informations about dev and subdomains of this server.

---

## **2. Edit informations**

It's possible to add informations about **social** (network, e-mail, CV...), **Git accounts** and **subdomains links**

```js
// assets/js/editable-infos.js

// Social infos
var social = [
    // ...
    {
        // domaine of social item
        url: 'https://www.domain.com',
        // font awesome icon https://fontawesome.com/
        icon: '<i class="fas fa-question-circle"></i>',
        // name of this link
        title: 'Title'
    }
];

// Git accounts infos
var git = [
    // ...
    {
        // domain of git account
        url: 'https://github.com/user',
        // font awesome icon https://fontawesome.com/
        icon: '<i class="fab fa-github"></i>',
        // name of this link
        title: 'GitHub'
    }
];

// Subdomains links
var links = [
    // ...
    {
        // url of git repository
        gitRepoUrl: 'https://github.com/ewilan-riviere/host-main-domain',
        // font awesome icon https://fontawesome.com/
        gitIcon: '<i class="fab fa-github"></i>',
        // link of subdomain
        link: 'https://ewilan-riviere.tech',
        // name of subdomain
        title: 'ewilan-riviere.tech',
        // description title
        textTitle: 'Domaine principal',
        // description details
        text: 'Le domaine principal répertoriant les domaines existants.'
    },
];
```

---

## **3. NodeJS Commands**

```bash
# Compile sass
npm run sass
```