// Infos about dev

var social = [
    {
        url: 'https://www.linkedin.com/in/ewilan-riviere/',
        icon: '<i class="fab fa-linkedin"></i>',
        title: 'LinkedIn'
    },
    {
        url: 'mailto:ewilan@dotslashplay.it',
        icon: '<i class="fas fa-envelope"></i>',
        title: 'E-mail'
    },
    {
        url: 'http://portfolio-api.ewilan-riviere.tech/documents/cv.pdf',
        icon: '<i class="fas fa-graduation-cap"></i>',
        title: 'CV'
    }
];

// Infos about dev git repos

var git = [
    {
        url: 'https://gitlab.com/EwieFairy',
        icon: '<i class="fab fa-gitlab"></i>',
        title: 'GitLab'
    },
    {
        url: 'https://github.com/ewilan-riviere',
        icon: '<i class="fab fa-github"></i>',
        title: 'GitHub'
    }
];

// Subdomaines links

var links = [
    {
        gitRepoUrl: 'https://github.com/ewilan-riviere/host-main-domain',
        gitIcon: '<i class="fab fa-github"></i>',
        link: 'https://ewilan-riviere.tech',
        title: 'ewilan-riviere.tech',
        textTitle: 'Domaine principal',
        text: 'Le domaine principal répertoriant les domaines existants. Réalisé sans framework et déployable facilement.'
    },
    {
        gitRepoUrl: 'https://gitlab.com/code-academie/promo-03/apprenants/site-promo-3',
        gitIcon: '<i class="fab fa-gitlab"></i>',
        link: 'http://promo3.ewilan-riviere.tech',
        title: 'promo3.ewilan-riviere.tech',
        textTitle: 'Promo 03 de la Code Académie',
        text: 'Version de développement du site de la promo 03 de la Code Académie. Réalisé en Laravel par une équipe de la promo 03.'
    },
    {
        gitRepoUrl: 'https://gitlab.com/EwieFairy/hacked-movies',
        gitIcon: '<i class="fab fa-gitlab"></i>',
        link: 'http://hacked-movies.ewilan-riviere.tech',
        title: 'hacked-movies.ewilan-riviere.tech',
        textTitle: 'Project Hacked Movies',
        text: 'Liste de films répertoriés. Réalisé en VueJS.'
    },
    {
        gitRepoUrl: 'https://gitlab.com/EwieFairy/hacked-movies-api',
        gitIcon: '<i class="fab fa-gitlab"></i>',
        link: 'http://hacked-movies-api.ewilan-riviere.tech',
        title: 'hacked-movies-api.ewilan-riviere.tech',
        textTitle: 'API du projet Hacked Movies',
        text: 'API pour l\'application du même nom. Réalisé en Laravel.'
    },
    {
        gitRepoUrl: 'https://gitlab.com/Norahenn/monstera-front',
        gitIcon: '<i class="fab fa-gitlab"></i>',
        link: 'http://monstera.ewilan-riviere.tech',
        title: 'monstera.ewilan-riviere.tech',
        textTitle: 'Projet Monstera, jeu de point & click',
        text: 'Jeu de point & click. Réalisé en ReactJS par Monstera Team.'
    },
    {
        gitRepoUrl: 'https://gitlab.com/EwieFairy/monstera-back',
        gitIcon: '<i class="fab fa-gitlab"></i>',
        link: 'http://monstera-api.ewilan-riviere.tech',
        title: 'monstera-api.ewilan-riviere.tech',
        textTitle: 'API du projet Monstera',
        text: 'API pour l\'application du même nom. Réalisé en Laravel.'
    },
    {
        gitRepoUrl: 'https://github.com/ewilan-riviere/ewilan-riviere-portfolio-front',
        gitIcon: '<i class="fab fa-github"></i>',
        link: 'http://portfolio.ewilan-riviere.tech',
        title: 'portfolio.ewilan-riviere.tech',
        textTitle: 'Portfolio d\'Ewilan Rivière',
        text: 'Portfolio d\'Ewilan Rivière. Réalisé en NuxtJS par Ewilan Rivière.'
    },
    {
        gitRepoUrl: 'https://github.com/ewilan-riviere/ewilan-riviere-portfolio-back',
        gitIcon: '<i class="fab fa-github"></i>',
        link: 'http://portfolio-api.ewilan-riviere.tech',
        title: 'portfolio-api.ewilan-riviere.tech',
        textTitle: 'API du Portfolio d\'Ewilan Rivière',
        text: 'API pour l\'application du même nom. Réalisé en Laravel.'
    }
];