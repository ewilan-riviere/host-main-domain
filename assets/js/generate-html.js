var aboutSocial = byID('aboutSocial')
for (let index = 0; index < social.length; index++) {
    const element = social[index];
    aboutSocial.innerHTML += '<a class="waves-effect waves-light font-weight-bold btn-large materialize-button mr-2 mt-2" target="_blank" href="'+element.url+'"><i class="material-icons left">'+element.icon+'</i>'+element.title+'</a>';
}

var aboutGit = byID('aboutGit')
for (let index = 0; index < git.length; index++) {
    const element = git[index];
    aboutGit.innerHTML += '<a class="waves-effect waves-light font-weight-bold btn-large materialize-button mr-2 mt-2" target="_blank" href="'+element.url+'"><i class="material-icons left">'+element.icon+'</i>'+element.title+'</a>';
}

var domainLinks = byID('domainLinks')
for (let index = 0; index < links.length; index++) {
    const element = links[index];

    domainLinks.innerHTML +=  '<li class="collection-item avatar"><img alt="open-graph-image" class="circle open-graph-image"><a href="'+element.link+'" target="_blank" class="title">'+element.title+'</a><div class="collection-details"><p>'+element.textTitle+'</p><div class="collection-text">'+element.text+'</div></div><a href="'+element.gitRepoUrl+'" target="_blank" class="secondary-content"><i class="material-icons">'+element.gitIcon+'</i></a>';
}
for (let index = 0; index < links.length; index++) {
    const element = links[index];
    var openGraphImage = byClass('open-graph-image')
    openGraphImage[index].setAttribute("src", getOpenGraph(element.gitRepoUrl));
}

function getOpenGraph(og_url) {
    let urlEncoded = encodeURIComponent(og_url);
    let apiKey = '5f55aadf-63bf-4516-852d-e4ed9dc97c89';
    let requestUrl = 'https://opengraph.io/api/1.1/site/' + urlEncoded + '?app_id=' + apiKey;
    var json_obj = JSON.parse(Get(requestUrl));
    if (json_obj.error.message == 'Rate limit exceeded') {
        console.error('Rate limit exceeded for opengraph.io API')
        return 'assets/images/git-logo.png'
    } else {
        return json_obj.hybridGraph.image;
    }
}

function Get(yourUrl){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",yourUrl,false);
    Httpreq.send(null);
    return Httpreq.responseText;          
}