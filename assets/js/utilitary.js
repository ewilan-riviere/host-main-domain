/**
 * shortcut for document.getElementById(id) method
 * @param elementID String that specifies the ID value. Case-insensitive.
 * @return Returns a reference to the first object with the specified value of the ID attribute.
 */
function byID(id) {
	let elementID = document.getElementById(id);
	return elementID;
}

/**
 * Shortcut for document.getElementsByClassName(class) method, this returns a HTMLCollection of the elements in the object on which the method was invoked (a document or an element) that have all the classes given by classNames. The classNames argument is interpreted as a space-separated list of classes.
 * @param elementClass String that specifies the class value. Case-insensitive.
 * @return Returns a reference to the first object with the specified value of the class attribute.
 */
function byClass(classe) {
	let elementClass = document.getElementsByClassName(classe);
	return elementClass;
}

function byTag(tag) {
	let elementTag = document.getElementsByTagName(tag);
	return elementTag;
}

function bySelect(select) {
	let elementSelect = document.querySelectorAll(select);
	return elementSelect;
}

function log(el) {
	if (el != null) {
		return console.log(el);
	}
}

function fadeIn(el, time) {
	el.style.opacity = 0;

	var last = +new Date();
	var tick = function () {
		el.style.opacity = +el.style.opacity + (new Date() - last) / time;
		last = +new Date();

		if (+el.style.opacity < 1) {
			(window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
		}
	};

	tick();
}

function show(select) {
	let element = byID(select);
	element.classList.remove('not-visible');
}
function hide(select) {
	let element = byID(select);
	element.classList.add('not-visible');
}

var HTMLcontent;
function showDelete(select) {
	let element = byID(select);
	log(HTMLcontent)
	element.innerHTML = HTMLcontent;
}
function hideDelete(select) {
	let element = byID(select);
	HTMLcontent = element;
	element.innerHTML = "";
}