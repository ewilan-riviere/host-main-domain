navbarActive('home');

function switchPage(id) {
    let main = byTag('main');
    for (let index = 0; index < main.length; index++) {
        const element = main[index];
        element.classList.add('not-visible');
    }
    show(id+'Page')
    navbarActive(id);
}