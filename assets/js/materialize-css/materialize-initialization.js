document.addEventListener('DOMContentLoaded', function() {
    var sidenav = document.querySelectorAll('.sidenav');
    var instanceSidenav = M.Sidenav.init(sidenav);
    
    var tabs = document.querySelectorAll('.tabs');
    var instanceTabs = M.Tabs.init(tabs);

    var tooltips = document.querySelectorAll('.tooltipped');
    var instanceTooltips = M.Tooltip.init(tooltips);

    var scrollspyOptions = {
        'scrollOffset': 0
    }
    var scrollspy = document.querySelectorAll('.scrollspy');
    var instanceScrollspy = M.ScrollSpy.init(scrollspy, scrollspyOptions);
});